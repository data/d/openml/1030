# OpenML dataset: ERA

https://www.openml.org/d/1030

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

1. Title: Employee Rejection\Acceptance  (Orinal ERA)

2. Source Informaion:
Donor: Arie Ben David
MIS, Dept. of Technology Management
Holon Academic Inst. of Technology
52 Golomb St.
Holon 58102
Israel
abendav@hait.ac.il
Owner: Yoav Ganzah
Business Administration School
Tel Aviv Univerity
Ramat Aviv 69978
Israel

3. Past Usage:

4. Relevant Information
The ERA data set was originally gathered during an academic decision-making
experiment aiming at determining which are the most important qualities of
candidates for a certain type of jobs. Unlike the ESL data set (enclosed)
which was collected from expert recruiters, this data set was collected
during a MBA academic course.
The input in the data set are features of a candidates such as past
experience, verbal skills, etc., and the output is the subjective judgment of
a decision-maker to which degree he or she tends to accept the applicant to
the job or to reject him altogether (the lowest score means total tendency to
reject an applicant and vice versa).

5. Number of Instances: 1000

6. Number of Attributes: 4 input, 1 output.

7. Attribute Information: All input and output values are ORDINAL.

8. Missing Attribute Values: None.

9. Class Distribution:

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1030) of an [OpenML dataset](https://www.openml.org/d/1030). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1030/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1030/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1030/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

